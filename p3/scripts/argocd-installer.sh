#/bin/bash

GREEN='\033[0;32m'
YELLOW='\033[0;33m'
NC='\033[0m'

# refs:
# https://argo-cd.readthedocs.io/en/stable/getting_started/

# install argocd
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml

# argocd cli installation
argocd > /dev/null 2>&1
if [ $? -ne 0 ]
then 

	echo "${YELLOW}ARGOCD CLI INSTALLATION ...${NC}"
	
	# add -s curl command for silence output
	sudo curl -SL -o /usr/local/bin/argocd https://github.com/argoproj/argo-cd/releases/latest/download/argocd-linux-amd64
	sudo chmod +x /usr/local/bin/argocd

else
	echo "${GREEN}argocd cli is already installed.${NC}"
fi

# change namespace default to > argocd
kubectl config set-context --current --namespace=argocd


RESPONSE_LENGTH=0
REFUSED=0
ENTITY_TO_WAIT=$(/usr/local/bin/kubectl get all | grep " 0/" | wc -l 2>/dev/null)
echo "wait [pods/services/deployments] for start ingress..."
while [ $RESPONSE_LENGTH -le 3 -o $REFUSED -ne 0 -o $ENTITY_TO_WAIT -ne 0 ]
do
    sleep 10

    RESPONSE=$(/usr/local/bin/kubectl get all 2>/dev/null)
    ENTITY_TO_WAIT=$(echo $RESPONSE | grep " 0/" | wc -l 2>/dev/null)
    RESPONSE_LENGTH=$(echo ${#RESPONSE} 2>/dev/null)
    REFUSED=$(echo $RESPONSE | grep "was refused" | wc -l 2>/dev/null)
done


# define port fowarding for argocd api server access in background
kubectl port-forward svc/argocd-server -n argocd 8080:443 &

# get default admin password
while [ -z "$DEFAULTPASSWORD" ];
do
	sleep 10
	export DEFAULTPASSWORD=$(kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d && echo) 2>/dev/null
done

# connection to argocd server
argocd login localhost:8080 --insecure --password ${DEFAULTPASSWORD} --username admin

# change the default admin password argocd api server
argocd account update-password --current-password ${DEFAULTPASSWORD} --new-password ${NEWPASSWORD}

# define name context for argocd
CLUSTERNAME=$(kubectl config get-contexts -o name)

# register a cluster to deploy app
# argocd cluster add -y ${CLUSTERNAME}

# set project/app params
export APPNAME=inceptionapp
export GITREPO=https://github.com/trixky/inception_manifest_mabois
export NAMESPACE=dev
export PROJECTNAME=development
export DESTSERVER=https://kubernetes.default.svc

# create project from git repositor
argocd proj create ${PROJECTNAME} -d ${DESTSERVER},${NAMESPACE} -s ${GITREPO}

# create application the project

argocd app create ${APPNAME} --repo ${GITREPO} --path ${APPNAME} --project ${PROJECTNAME} --dest-server ${DESTSERVER} --sync-policy automated --dest-namespace ${NAMESPACE}
