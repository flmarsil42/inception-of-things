#!/bin/bash

GREEN='\033[0;32m'
YELLOW='\033[0;33m'
NC='\033[0m'

# testing kubectl installation
kubectl > /dev/null 2>&1
if [ $? -ne 0 ]
then
	echo "${YELLOW}KUBECTL INSTALLATION ...${NC}"
	# Download the latest release
	curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
	# Install kubectl
	sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl

else
	echo "${GREEN}kubectl is already installed.${NC}"
fi

# https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/
