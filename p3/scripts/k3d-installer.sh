#!/bin/bash

GREEN='\033[0;32m'
YELLOW='\033[0;33m'
NC='\033[0m'

# testing k3d installation
k3d > /dev/null 2>&1
if [ $? -ne 0 ]
then
	echo "${YELLOW}K3D INSTALLATION ...${NC}"

	# install k3d last release
	curl -s https://raw.githubusercontent.com/rancher/k3d/main/install.sh | bash
else
	echo "${GREEN}k3d is already installed.${NC}"
fi

# https://k3d.io/v5.0.1/#installation
