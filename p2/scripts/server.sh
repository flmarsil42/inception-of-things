#!/bin/sh

# ressources:
#   * https://www.grottedubarbu.fr/installation-de-k3s-sur-un-vps-ovh/

# install k3s /kubectl
# [--write-kubeconfig-mode 644] arg resolves the "open /etc/rancher/k3s/k3s.yaml: permission denied" error
# ^ https://github.com/k3s-io/k3s/issues/389 ^

# other ressources:
# https://www.it-swarm-fr.com/fr/mysql/comment-se-connecter-au-serveur-mysql-dans-virtualbox-vagrant/1066932566/
# https://stackoverflow.com/questions/66449289/is-there-any-way-to-bind-k3s-flannel-to-another-interface
# https://github.com/biggers/vagrant-kubernetes-by-k3s/blob/master/Vagrantfile
# https://tferdinand.net/creer-un-cluster-kubernetes-local-avec-vagrant/

echo "hello world! I'm the Server"

# install ifconfig
yum install net-tools -y

# install helm
# https://helm.sh/docs/intro/install/
curl https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash


# configure hosts
echo "127.0.1.1 $(hostname)" >> /etc/hosts

# get the current ip
current_ip=$(/sbin/ip -o -4 addr list eth1 | awk '{print $4}' | cut -d/ -f1)

# install k3s/kubectl and
# up a master server
curl -sfL https://get.k3s.io | INSTALL_K3S_EXEC="server --cluster-init --tls-san $(hostname) --bind-address=${current_ip} --advertise-address=${current_ip} --node-ip=${current_ip} --no-deploy=traefik --write-kubeconfig-mode 644" sh -

# fix the "Kubernetes cluster unreachable" error
# https://github.com/k3s-io/k3s/issues/1126
export KUBECONFIG=/etc/rancher/k3s/k3s.yaml

# install ingress-nginx
# https://www.digitalocean.com/community/tutorials/how-to-set-up-an-nginx-ingress-on-digitalocean-kubernetes-using-helm
# helm is not in $PATH at this step
/usr/local/bin/helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
/usr/local/bin/helm repo update
/usr/local/bin/helm install nginx-ingress ingress-nginx/ingress-nginx --set controller.publishService.enabled=true

# start the project config
# kubectl is not in $PATH at this step
/usr/local/bin/kubectl create -f /home/vagrant/config/app1.yml # app1
/usr/local/bin/kubectl create -f /home/vagrant/config/app2.yml # app2
/usr/local/bin/kubectl create -f /home/vagrant/config/app3.yml # app3

RESPONSE_LENGTH=0
REFUSED=0
ENTITY_TO_WAIT=$(/usr/local/bin/kubectl get all | grep " 0/" | wc -l 2>/dev/null)
echo "wait [pods/services/deployments] for start ingress..."
while [ $RESPONSE_LENGTH -le 3 -o $REFUSED -ne 0 -o $ENTITY_TO_WAIT -ne 0 ]
do
    sleep 10

    RESPONSE=$(/usr/local/bin/kubectl get all 2>/dev/null)
    ENTITY_TO_WAIT=$(echo $RESPONSE | grep " 0/" | wc -l 2>/dev/null)
    RESPONSE_LENGTH=$(echo ${#RESPONSE} 2>/dev/null)
    REFUSED=$(echo $RESPONSE | grep "was refused" | wc -l 2>/dev/null)
done
sleep 10
/usr/local/bin/kubectl apply -f /home/vagrant/config/ingress.yml
