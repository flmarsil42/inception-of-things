#!/bin/sh

# ressources:
#   * https://www.grottedubarbu.fr/installation-de-k3s-sur-un-vps-ovh/

# install ifconfig

# install k3s /kubectl
# [--write-kubeconfig-mode 644] arg resolves the "open /etc/rancher/k3s/k3s.yaml: permission denied" error
# ^ https://github.com/k3s-io/k3s/issues/389 ^

# other ressources:
# https://www.it-swarm-fr.com/fr/mysql/comment-se-connecter-au-serveur-mysql-dans-virtualbox-vagrant/1066932566/
# https://stackoverflow.com/questions/66449289/is-there-any-way-to-bind-k3s-flannel-to-another-interface
# https://github.com/biggers/vagrant-kubernetes-by-k3s/blob/master/Vagrantfile

echo "hello world! I'm the Server_worker"

# config environment
hostname=$(hostname)

# install ifconfig
sudo yum install net-tools -y

# configure ssh
mkdir /root/.ssh/
mv /tmp/id_rsa*  /root/.ssh/
chmod 400 /root/.ssh/id_rsa*
chown root:root  /root/.ssh/id_rsa*

cat /root/.ssh/id_rsa.pub >> /root/.ssh/authorized_keys
chmod 400 /root/.ssh/authorized_keys
chown root:root /root/.ssh/authorized_keys

# configure hosts
echo "127.0.1.1 $(hostname)" >> /etc/hosts
echo "192.168.42.110 maboisS" >> /etc/hosts

# get the current ip
current_ip=$(/sbin/ip -o -4 addr list eth1 | awk '{print $4}' | cut -d/ -f1)

# get the k3s token from the master server
scp -o StrictHostKeyChecking=no root@maboisS:/var/lib/rancher/k3s/server/token /tmp/token

# install k3s/kubectl and
# up a worker server
curl -sfL https://get.k3s.io | INSTALL_K3S_EXEC="agent --server https://maboisS:6443 --token-file /tmp/token --node-ip=${current_ip}" sh -
